package pt.ipp.isep.dei.examples.tdd.basic.domain;


import java.util.Comparator;

public class RatingComparator implements Comparator<Bookmark> {

    public int compare(Bookmark b1, Bookmark b2){
        if(b1.getRating() > b2.getRating()){
            return 1;
        }else if(b1.getRating() < b2.getRating()){
            return -1;
        }else{
            return 0;
        }
    }
}



